import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { ConfigModule } from '@nestjs/config'
import { LoggerModule } from 'nestjs-pino'
import app from '../config/app'
import database from '../config/database'
import { PlainConverterModule } from '../api/plain-converter/plain-converter.module'

@Module({
  imports: [
    LoggerModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [app, database]
      // ignoreEnvFile: false
    }),
    PlainConverterModule
  ],
  providers: [],
  controllers: [AppController]
})

export class AppModule {
}
