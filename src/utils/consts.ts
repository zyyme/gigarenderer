import { RenderFormats } from './types'

export enum ContentTypeFormats {
  'pdf' = 'application/pdf',
  'png' = 'image/png',
  'svg' = 'image/svg+xml',
  'svg.editable' = 'image/svg+xml',
  'gif' = 'image/gif',
  'json' = 'application/json',
  'eps' = 'application/json',
  'aep' = 'application/json'
}

export const GET_BY_ID_COMMAND = 'idCommand'

export const GET_BY_NAME_AND_PLATFORM_COMMAND = 'nameAndPlatformCommand'

export const PNG_PREVIEW_SIZE = 128

export const ANIMATED_FORMATS = [RenderFormats.gif, RenderFormats.aep, RenderFormats.json]
