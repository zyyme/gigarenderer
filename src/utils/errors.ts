import { HttpStatus } from '@nestjs/common'

interface APIErrorDefinition {
  name?: string
  status?: number
  code?: string
  message?: string
}

export class APIError<Data = any, ClientData = any> extends Error {
  status!: number
  code!: string
  reason?: Error
  data?: Data
  clientData?: ClientData

  constructor (definition?: APIErrorDefinition) {
    const message = definition?.message ?? 'Something went wrong'
    super(message)

    if (definition) {
      this.name = definition.name ?? 'Error'
      this.status = definition.status ?? 500
      this.code = definition.code ?? 'NOT_FOUND_CODE'
      this.message = definition.message ?? 'Something went wrong'
    }

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor)
    } else {
      this.stack = new Error().stack
    }
  }

  withReason (reason: Error): APIError {
    this.reason = reason
    return this
  }

  withData (data: Data) : APIError {
    this.data = data
    return this
  }

  withClientData (data: ClientData) : APIError {
    this.clientData = data
    return this
  }
}

export const errors = {
  COMMON_ICON_NOT_FOUND: () => new APIError({
    status: HttpStatus.NOT_FOUND,
    code: 'COMMON_ICON_NOT_FOUND',
    message: 'common icon not found'
  }),
  ICON_NAME_NOT_FOUND: (name: string) => new APIError({
    status: HttpStatus.NOT_FOUND,
    code: 'ICON_NAME_NOT_FOUND',
    message: `Icon ${name} not found`
  }),
  PLATFORM_ICON_NOT_FOUND: (name: string, platform: string) => new APIError({
    status: HttpStatus.NOT_FOUND,
    code: 'PLATFORM_ICON_NOT_FOUND',
    message: `Icon ${name} in ${platform} styles not found`
  }),
  ICON_SOURCE_NOT_FOUND: () => new APIError({
    status: HttpStatus.NOT_FOUND,
    code: 'ICON_SOURCE_NOT_FOUND',
    message: 'icon source not found'
  }),
  ICON_NOT_ANIMATED: () => new APIError({
    status: HttpStatus.BAD_REQUEST,
    code: 'ICON_NOT_ANIMATED',
    message: 'icon is not animated'
  }),
  ID_NOT_SPECIFIED: () => new APIError({
    status: HttpStatus.BAD_REQUEST,
    code: 'ID_NOT_SPECIFIED',
    message: 'icon id not specified'
  }),
  UNKNOWN_FORMAT: () => new APIError({
    status: HttpStatus.BAD_REQUEST,
    code: 'UNKNOWN_FORMAT',
    message: 'format provided is unknown'
  })
}
