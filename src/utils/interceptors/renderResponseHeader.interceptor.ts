import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

export class RenderResponseHeaderInterceptor<T> implements NestInterceptor<T, Response> {
  intercept (context: ExecutionContext, next: CallHandler): Observable<Response> {
    return next.handle().pipe(map(data => {
      const response = context.switchToHttp().getResponse()

      response.headers(data.headers)
      response.type(data.headers['Content-Type']).send(data.data)
      return data.data
    }))
  }
}
