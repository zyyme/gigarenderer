import { CacheInterceptor, CallHandler, ExecutionContext, Injectable } from '@nestjs/common'
import { catchError, forkJoin, Observable, of, switchMap } from 'rxjs'
import { map } from 'rxjs/operators'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class CustomCacheInterceptor extends CacheInterceptor {
  constructor (
    cacheManager: any,
    reflector: any,
    private readonly configService: ConfigService
  ) {
    super(cacheManager, reflector)
  }

  async intercept (context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
    const http = context.switchToHttp()
    const request = http.getRequest()

    const key = request.url

    const cached = await this.cacheManager.get(key)

    if (cached != null) {
      const response = context.switchToHttp().getResponse()

      const { renderResult, headers } = this.processRedisCacheEntry(cached)

      headers['from-redis-cache'] = true
      headers['from-mongo-cache'] = false

      response.headers(headers)
      return of(renderResult)
    }

    return next.handle().pipe(
      switchMap(result => {
        const response = context.switchToHttp().getResponse()
        const cachedData = {
          renderResult: result,
          headers: response.getHeaders()
        }
        return forkJoin(
          of(result),
          this.cacheManager.set(key, cachedData)
        ).pipe(catchError(e => of(result)))
      }),
      map(([result, setOp]) => result)
    )
  }

  private processRedisCacheEntry (rawData) {
    rawData.renderResult = Buffer.from(rawData.renderResult.data)
    return rawData
  }
}
