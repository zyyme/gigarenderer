import { instance, mock, when } from 'ts-mockito'
import { ConfigService } from '@nestjs/config'
import { expect } from '../tests'
import { PlainConverterPipe } from './plainConverter.pipe'
import { RenderFormats } from '../types'

describe('getByParams pipe', () => {
  const ConfigServiceMock = mock(ConfigService)
  when(ConfigServiceMock.get<number>('defaultSize')).thenReturn(64)
  when(ConfigServiceMock.get<string>('defaultFormat')).thenReturn('png')

  const plainConverterPipe = new PlainConverterPipe(
    instance(ConfigServiceMock)
  )

  describe('parses request', () => {
    it('parses full request correctly', () => {
      const request = {
        id: 'id',
        svg: '<svg></svg>',
        format: RenderFormats.png as const,
        size: 256,
        color: 'ff0000'
      }

      const result = plainConverterPipe.transform(request)

      expect(result).deep.eq(request)
    })

    it('sets default size when no size specified', () => {
      const request = {
        id: 'id',
        svg: '<svg></svg>',
        format: RenderFormats.png as const,
        color: 'ff0000'
      }

      const result = plainConverterPipe.transform(request)

      expect(result.size).deep.eq(64)
    })

    it('sets default format when no format specified', () => {
      const request = {
        id: 'id',
        svg: '<svg></svg>',
        size: 128,
        color: 'ff0000'
      }

      const result = plainConverterPipe.transform(request)

      expect(result.format).deep.eq('png')
    })
  })
})
