import { Injectable, PipeTransform, Type } from '@nestjs/common'
import { isHexColor } from '../isHexColor'
import { ConfigService } from '@nestjs/config'
import { GET_BY_NAME_AND_PLATFORM_COMMAND } from '../consts'
import { RenderByNamePlatformCommand, RenderFormats } from '../types'
import { errors } from '../errors'

interface ArgumentMetadata {
  type: 'param'
  metatype?: Type<unknown>
  data?: string
}

@Injectable()
export class GetByParamsPipe implements PipeTransform {
  constructor (
    private readonly configService: ConfigService
  ) {
  }

  // todo: documentation on how path may look
  transform (value: any, metadata?: ArgumentMetadata): RenderByNamePlatformCommand {
    let name, version, platform, format, size, color
    let formatParts
    const availableFormats = Object.values(RenderFormats)

    const rawParams = value.split('/')

    for (const [index, param] of rawParams.entries()) {
      // extracting name
      if (index === rawParams.length - 1) {
        [name, ...formatParts] = param.split('.')

        if (name.indexOf('--v') !== -1) {
          [name, version] = name.split('--v')
          version = Number(version)
        }

        if (formatParts.length === 0) {
          format = this.configService.get<string>('defaultFormat')
        } else {
          if (formatParts.length > 1) {
            // this part is needed to correctly parse icon names that contain dots, like android.os.svg.editable
            if (availableFormats.includes(formatParts.slice(formatParts.length - 2).join('.'))) {
              name += '.' + formatParts.splice(0, formatParts.length - 2).join('.')
            } else {
              name += '.' + formatParts.splice(0, formatParts.length - 1).join('.')
            }
            format = formatParts.join('.')
          } else {
            format = formatParts[0]
          }
          format = formatParts.join('.')
        }

        if (!Object.values(RenderFormats).includes(format)) {
          throw errors.UNKNOWN_FORMAT()
        }
        continue
      }

      // extracting size
      if (
        (param.endsWith('x') && Number(param.slice(0, -1))) ||
        (Number(param) && param.length !== 6)
      ) {
        size = param
        continue
      }

      // extracting colors
      if (param.length === 6) {
        const hex = '#' + param
        if (isHexColor(hex)) {
          // if we have gradient platform, then we will need to extract multiple colors
          if (platform && platform === 'nolan') {
            if (color) {
              if (Array.isArray(color)) {
                color.push(param)
              }
            } else {
              color = [param]
            }
          } else {
            color = param
          }

          continue
        }
      }

      // extracting platform -- platform will be anything that does not fit previous conditions
      if (!platform) {
        platform = param
      }
    }

    if (!platform) platform = this.configService.get<string>('defaultPlatform')

    return {
      name,
      platform,
      format,
      size: size || this.configService.get<number>('defaultSize'),
      color,
      version,
      kind: GET_BY_NAME_AND_PLATFORM_COMMAND
    }
  }
}
