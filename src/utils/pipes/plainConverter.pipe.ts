import { Injectable, PipeTransform, Type } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { RenderByCustomSvgCommandDto } from '../../api/plain-converter/dto/renderByCustomSvgCommandDto'
import { RenderByCustomSvgCommand } from '../../api/plain-converter/dto/renderByCustomSvgCommand'

interface ArgumentMetadata {
  type: 'body'
  metatype?: Type<unknown>
  data?: any
}

@Injectable()
export class PlainConverterPipe implements PipeTransform<RenderByCustomSvgCommandDto, RenderByCustomSvgCommand> {
  constructor (
    private readonly configService: ConfigService
  ) {
  }

  transform (value: RenderByCustomSvgCommandDto, metadata?: ArgumentMetadata): RenderByCustomSvgCommand {
    return {
      id: value.id,
      svg: value.svg,
      format: value.format || (this.configService.get<string>('defaultFormat') as any),
      size: value.size || this.configService.get<number>('defaultSize'),
      color: value.color
    }
  }
}
