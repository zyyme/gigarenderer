import { expect } from '../tests'
import { GetByIdPipe } from './getById.pipe'
import { instance, mock, when } from 'ts-mockito'
import { ConfigService } from '@nestjs/config'
import { GET_BY_ID_COMMAND } from '../consts'
import { RenderFormats } from '../types'

describe('getByParams pipe', () => {
  const ConfigServiceMock = mock(ConfigService)
  when(ConfigServiceMock.get<string>('defaultFormat')).thenReturn('png')

  const getByParamsPipe = new GetByIdPipe(
    instance(ConfigServiceMock)
  )

  describe('parsing request', () => {
    it('parses full request with exact size correctly', () => {
      const params = {
        id: 'id',
        format: RenderFormats.png,
        size: '128',
        color: 'ff0000'
      }

      const result = getByParamsPipe.transform(params)

      expect(result).deep.eq({
        id: 'id',
        format: RenderFormats.png,
        size: '128',
        color: 'ff0000',
        kind: GET_BY_ID_COMMAND
      })
    })

    it('parses full request with pixel perfect size correctly', () => {
      const params = {
        id: 'id',
        format: RenderFormats.png,
        size: '2x',
        color: 'ff0000'
      }

      const result = getByParamsPipe.transform(params)

      expect(result).deep.eq({
        id: 'id',
        format: RenderFormats.png,
        size: '2x',
        color: 'ff0000',
        kind: GET_BY_ID_COMMAND
      })
    })

    it('parses multiple colors correctly', () => {
      const params = {
        id: 'id',
        format: RenderFormats.png,
        size: '128',
        color: 'ffffff,000000'
      }

      const result = getByParamsPipe.transform(params)

      expect(result.color).deep.eq(['ffffff', '000000'])
    })

    it('sets default format when no format specified', () => {
      const params = {
        id: 'id',
        size: '128',
        color: 'ffffff'
      }

      const result = getByParamsPipe.transform(params)

      expect(result.format).deep.eq(RenderFormats.png)
    })
  })
})
