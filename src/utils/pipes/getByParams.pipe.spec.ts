import { instance, mock, when } from 'ts-mockito'
import { ConfigService } from '@nestjs/config'
import { GetByParamsPipe } from './getByParams.pipe'
import { expect } from '../tests'
import { RenderFormats } from '../types'
import { GET_BY_NAME_AND_PLATFORM_COMMAND } from '../consts'
import { errors } from '../errors'

describe('getByParams pipe', () => {
  const ConfigServiceMock = mock(ConfigService)
  when(ConfigServiceMock.get<string>('defaultPlatform')).thenReturn('defaultPlatform')
  when(ConfigServiceMock.get<string>('defaultFormat')).thenReturn('png')
  when(ConfigServiceMock.get<number>('defaultSize')).thenReturn(100)

  const getByParamsPipe = new GetByParamsPipe(
    instance(ConfigServiceMock)
  )

  describe('parsing request', () => {
    it('correctly parses full request', () => {
      const request = 'ios7/ff0000/2x/icon.svg'

      const result = getByParamsPipe.transform(request)

      expect(result).deep.eq({
        name: 'icon',
        platform: 'ios7',
        format: 'svg',
        size: '2x',
        color: 'ff0000',
        kind: GET_BY_NAME_AND_PLATFORM_COMMAND,
        version: undefined
      })
    })

    it('correctly parser request with icon version', () => {
      const request = 'ios7/ff0000/2x/icon--v2.svg'

      const result = getByParamsPipe.transform(request)

      expect(result).deep.eq({
        name: 'icon',
        platform: 'ios7',
        format: 'svg',
        size: '2x',
        color: 'ff0000',
        kind: GET_BY_NAME_AND_PLATFORM_COMMAND,
        version: 2
      })
    })

    it('correctly parses icon name with dot in it in singular format', () => {
      const request = 'ios7/ff0000/2x/icon.icon.svg'

      const result = getByParamsPipe.transform(request)

      expect(result.name).deep.eq('icon.icon')
      expect(result.format).deep.eq('svg')
    })

    it('correctly parses icon name with dot in it in svg.editable format', () => {
      const request = 'ios7/ff0000/2x/icon.icon.svg.editable'

      const result = getByParamsPipe.transform(request)

      expect(result.name).deep.eq('icon.icon')
      expect(result.format).deep.eq('svg.editable')
    })

    it('correctly parses full request with different order and pixel perfect size', () => {
      const requestParts = ['ios7', 'ff0000', '2x']
      const permutations = perm(requestParts).map(x => x.join('/'))

      const transformResult = {
        name: 'icon',
        platform: 'ios7',
        format: 'svg',
        size: '2x',
        color: 'ff0000',
        kind: GET_BY_NAME_AND_PLATFORM_COMMAND,
        version: undefined
      }

      for (const permutation of permutations) {
        const request = permutation + '/icon.svg'

        const result = getByParamsPipe.transform(request)

        expect(result).deep.eq(transformResult)
      }
    })

    it('correctly parses full request with different order and exact size', () => {
      const requestParts = ['ios7', 'ff0000', '128']
      const permutations = perm(requestParts).map(x => x.join('/'))

      const transformResult = {
        name: 'icon',
        platform: 'ios7',
        format: 'svg',
        size: '128',
        color: 'ff0000',
        kind: GET_BY_NAME_AND_PLATFORM_COMMAND,
        version: undefined
      }

      for (const permutation of permutations) {
        const request = permutation + '/icon.svg'

        const result = getByParamsPipe.transform(request)

        expect(result).deep.eq(transformResult)
      }
    })

    it('correctly parses request with pixel perfect size', () => {
      const request = 'ios7/ff0000/2x/icon.svg'

      const result = getByParamsPipe.transform(request)

      expect(result.size).deep.eq('2x')
    })

    it('correctly parses request with exact size', () => {
      const request = 'ios7/ff0000/128/icon.svg'

      const result = getByParamsPipe.transform(request)

      expect(result.size).deep.eq('128')
    })

    it('correctly parses request with singular format', () => {
      const formats = Object.values(RenderFormats)

      for (const format of formats) {
        if (format === RenderFormats.svgEditable) continue

        const request = `ios7/ff0000/128/icon.${format}`
        const result = getByParamsPipe.transform(request)
        expect(result.format).deep.eq(format)
      }
    })

    it('correctly parses request with double extension format', () => {
      const request = 'ios7/ff0000/128/icon.svg.editable'

      const result = getByParamsPipe.transform(request)

      expect(result.format).deep.eq('svg.editable')
    })

    it('correctly parses request with single color', () => {
      const request = 'ios7/ff0000/128/icon.svg.editable'

      const result = getByParamsPipe.transform(request)

      expect(result.color).deep.eq('ff0000')
    })

    it('correctly parses request with multiple colors on gradient platform', () => {
      const request = 'nolan/ff0000/00ff00/128/icon.svg.editable'

      const result = getByParamsPipe.transform(request)

      expect(result.color).deep.eq(['ff0000', '00ff00'])
    })

    it('gets last color on request with non-gradient platform', () => {
      const request = 'ios7/ff0000/00ff00/128/icon.svg.editable'

      const result = getByParamsPipe.transform(request)

      expect(result.color).deep.eq('00ff00')
    })

    it('correctly parses platform', () => {
      const request = 'ios7/ff0000/128/icon.svg.editable'

      const result = getByParamsPipe.transform(request)

      expect(result.platform).deep.eq('ios7')
    })

    it('sets default platform if none present', () => {
      const request = 'ff0000/128/icon.svg.editable'

      const result = getByParamsPipe.transform(request)

      expect(result.platform).deep.eq('defaultPlatform')
    })

    it('sets default format if none present', () => {
      const request = 'ff0000/128/icon'

      const result = getByParamsPipe.transform(request)

      expect(result.format).deep.eq('png')
    })

    it('sets default size if none present', () => {
      const request = 'ff0000/icon.png'

      const result = getByParamsPipe.transform(request)

      expect(result.size).deep.eq(100)
    })

    it('does not set color if none present', () => {
      const request = '128/icon.png'

      const result = getByParamsPipe.transform(request)

      // eslint-disable-next-line no-unused-expressions
      expect(result.color).to.be.undefined
    })

    it('throws if wrong format is used', () => {
      const request = '128/icon.docx'

      try {
        const result = getByParamsPipe.transform(request)
      } catch (e) {
        expect(e.code).deep.eq(errors.UNKNOWN_FORMAT().code)
      }
    })
  })
})

function perm (xs) {
  const ret = []
  for (let i = 0; i < xs.length; i = i + 1) {
    const rest = perm(xs.slice(0, i).concat(xs.slice(i + 1)))
    if (!rest.length) {
      ret.push([xs[i]])
    } else {
      for (let j = 0; j < rest.length; j = j + 1) {
        ret.push([xs[i]].concat(rest[j]))
      }
    }
  }
  return ret
}
