import { Injectable, PipeTransform, Type } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { GET_BY_ID_COMMAND } from '../consts'
import { RenderByIdCommand, RenderByIdDto, RenderFormats } from '../types'

interface ArgumentMetadata {
  type: 'query'
  metatype?: Type<unknown>
  data?: any
}

@Injectable()
export class GetByIdPipe implements PipeTransform<RenderByIdDto, RenderByIdCommand> {
  constructor (
    private readonly configService: ConfigService
  ) {
  }

  transform (value: RenderByIdDto, metadata?: ArgumentMetadata): RenderByIdCommand {
    return {
      id: value.id,
      format: value.format || this.configService.get<RenderFormats>('defaultFormat'),
      size: value.size,
      color: this.formatColors(value.color),
      kind: GET_BY_ID_COMMAND
    }
  }

  private formatColors (colors: string) {
    if (colors) {
      const colorsArray = colors.split(',')

      return colorsArray.length > 1 ? colorsArray : colorsArray[0]
    }

    return undefined
  }
}
