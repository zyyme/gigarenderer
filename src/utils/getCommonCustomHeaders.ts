import { RenderByCustomSvgCommand } from '../api/plain-converter/dto/renderByCustomSvgCommand'
import { ContentTypeFormats } from './consts'
import * as dayjs from 'dayjs'
import { RenderFormats } from './types'

interface CommonHeaders {
  'Content-Type': string
  'Content-Disposition'?: string
  'icon-id'?: string
  'icon-size'?: number
  'icon-format'?: string
  'Last-Modified': string
  version: string
}

export function getCommonCustomHeaders (
  metadata: { id: string, format: string, size: number, updatedAt: Date } | RenderByCustomSvgCommand,
  filename = 'result'
): CommonHeaders {
  const lastModified = 'updatedAt' in metadata ? metadata.updatedAt : new Date()

  const defaultFilename = `result.${metadata.format}`

  return {
    'Content-Type': ContentTypeFormats[metadata.format],
    ...(!(metadata.format === RenderFormats.png || metadata.format === RenderFormats.svg)
      ? { 'Content-Disposition': `attachment; filename=${filename || defaultFilename}` }
      : {}),
    ...(metadata.id ? { 'icon-id': metadata.id, 'icon-size': metadata.size, 'icon-format': metadata.format } : {}),
    'Last-Modified': dayjs(lastModified).format('ddd, DD MMM YYYY HH:mm:ss'),
    version: '0.0.29'
  }
}
