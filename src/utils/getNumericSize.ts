import { ExactSize, PlatformInfo } from './types'

export function getNumericSize (size: string, platform: PlatformInfo): ExactSize {
  if (!size) {
    return platform.size
  }

  if (typeof (size) === 'string' && size.includes('x')) {
    const multiplier = Number.parseInt(size.split('x')[0])

    return platform.size * multiplier
  }

  return Number.parseInt(size)
}
