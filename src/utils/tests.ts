import { EventEmitter } from 'events'
import { FastifyInstance } from 'fastify'
import sinon from 'sinon'
import { mock } from 'ts-mockito'

const chai = require('chai')
const chaiHttp = require('chai-http')
const chaiAsPromised = require('chai-as-promised')
const sinonChai = require('sinon-chai')

chai.use(chaiHttp)
chai.use(chaiAsPromised)
chai.use(sinonChai)

const { expect } = chai

export function makeAppPromise (fastify: FastifyInstance, appState: EventEmitter): Promise<FastifyInstance['server']> {
  return new Promise(resolve => {
    appState.on('was/runned', () => resolve(fastify.server))
  })
}

export {
  chai,
  sinon,
  expect,

  // these are for typings to be automatically injected
  chaiHttp,
  chaiAsPromised,
  sinonChai
}

/**
 * Temporarily patch an object, using beforeEach and afterEach scoped hooks.
 */
export function patch<T, K extends keyof T> (obj: T, key: K, value: T[K]): void {
  const original = obj[key]
  beforeEach(() => {
    obj[key] = value
  })
  afterEach(() => {
    obj[key] = original
  })
}

/**
 * Make something before the test suite, and optionally remove later using a teardown function.
 * !!! Be careful to use returned values only in tests, otherwise typing is not valid.
 */
export function makeBefore<T> (
  before_: () => Promise<T>, after_?: (value: T) => Promise<void>, def: T = {} as T
): T {
  return _makeContextWrapper(before, after, before_, after_, def)
}

/**
 * Make something before each test, and optionally remove later using a teardown function.
 * !!! Be careful to use returned values only in tests, otherwise typing is not valid.
 */
export function makeBeforeEach<T> (
  before: () => Promise<T>, after?: (value: T) => Promise<void>, def: T = {} as T
): T {
  return _makeContextWrapper(beforeEach, afterEach, before, after, def)
}

/**
 * Make a `createApp` hook for route testing.
 */
export function makeCreateAppHook (fastify: FastifyInstance, appState: EventEmitter): () => FastifyInstance['server'] {
  const promise = makeAppPromise(fastify, appState)
  return () => makeBefore(async () => await promise)
}

function _makeContextWrapper<T> (
  beforeHook: (x: () => any) => void, afterHook: (x: () => any) => void,
  before: () => Promise<T>, after?: (value: T) => Promise<void>, def: T = {} as T
): T {
  let value: T = def

  beforeHook(async () => {
    value = await before()
  })

  afterHook(async () => {
    if (after) {
      await after(value)
    }
  })

  // @ts-ignore
  return new Proxy(value, {
    get: (_target, key) => {
      return value[key]
    },
    set: (_target, key, propValue) => {
      value[key] = propValue
      return true
    }
  })
}

/**
 * Mock an object every time a test starts.
 * `setupFn` will be called after creating the mock to setup ts-mockito `when(...).then(...)` handlers.
 */
export function mockBeforeEach<T extends object> (origin?: { new(...args: any[]): T }, setupFn?: (mock: T) => void): T
export function mockBeforeEach<T extends object> (origin?: T, setupFn?: (mock: T) => void): T
export function mockBeforeEach<T extends object> (origin?: any, setupFn?: (mock: T) => void): T
export function mockBeforeEach<T extends object> (origin?: any, setupFn?: (mock: T) => void): T {
  let value: T = {} as T

  beforeEach(() => {
    value = mock(origin)
    setupFn?.(value)
  })

  return new Proxy(value, {
    get: (_target, key) => {
      if (['Symbol(Symbol.toPrimitive)', 'then', 'catch'].includes(key.toString())) {
        return undefined
      }

      return value[key]
    },
    set: (_target, key, propValue) => {
      if (['Symbol(Symbol.toPrimitive)', 'then', 'catch'].includes(key.toString())) {
        return false
      }

      value[key] = propValue
      return true
    }
  })
}
