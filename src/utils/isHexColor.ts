export function isHexColor (colorString: string): boolean {
  const matcher = /^#[a-f0-9]{3}([a-f0-9]{3})?$/i

  return matcher.test(colorString)
}
