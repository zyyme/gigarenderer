import { GET_BY_ID_COMMAND, GET_BY_NAME_AND_PLATFORM_COMMAND } from './consts'

export enum RenderFormats {
  png = 'png',
  pdf = 'pdf',
  svg = 'svg',
  svgEditable = 'svg.editable',
  eps = 'eps',
  gif = 'gif',
  aep = 'aep',
  json = 'json'
}

export type HexColor = string
export type GradientColors = HexColor[]

export type MultiplierSize = string
export type PixelSize = string
export type InputSize = MultiplierSize | PixelSize

export type ExactSize = number
export type SVGSource = string

export interface PlatformInfo {
  apiCode: string
  seoCode: string
  size: number
}

export interface RenderParams {
  width: ExactSize
  height: ExactSize
  color: HexColor | GradientColors
  format: RenderFormats
  inputSize?: InputSize
  fromCache?: boolean
}

export interface IconData {
  _id: string
  name: string
  platform: PlatformInfo
  sourceFormat?: RenderFormats
  createdAt: Date
  updatedAt: Date
  isFree?: boolean
  isAnimated: boolean
  source?: SVGSource
  isColor?: boolean
}

export class RenderByIdDto {
  id: string
  format?: RenderFormats = RenderFormats.png
  size?: string
  color?: string
}

interface BaseRenderCommand {
  format: RenderFormats

  size?: InputSize

  color?: HexColor | GradientColors
}

export interface RenderByIdCommand extends BaseRenderCommand {
  kind: typeof GET_BY_ID_COMMAND
  id: string
}

export interface RenderByNamePlatformCommand extends BaseRenderCommand {
  kind: typeof GET_BY_NAME_AND_PLATFORM_COMMAND
  name: string
  platform: string
  version?: number
}
