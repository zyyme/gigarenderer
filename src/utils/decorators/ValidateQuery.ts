import { createParamDecorator, ExecutionContext, Type } from '@nestjs/common'
import { ValidationPipe } from '@nestjs/common/pipes/validation.pipe'

// We should use custom query decorator here to prevent validation of global validator
const CustomQuery = createParamDecorator<Type<any>>(
  (data: Type<any>, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest()
    return request.query
  })

export const ValidateQuery = (data: (Type<any> | { type: Type<any> }), ...pipes) => {
  const type = typeof data === 'object' ? data.type : data

  return CustomQuery(type,
    new ValidationPipe({
      expectedType: type,
      validateCustomDecorators: true,
      whitelist: true,
      transformOptions: { enableImplicitConversion: true }
    }),
    ...pipes
  )
}
