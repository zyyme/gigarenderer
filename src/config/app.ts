import * as path from 'path'
import * as gitVersion from 'git-tag-version'
import * as process from 'process'

export default () => {
  const env = process.env.NODE_ENV
  const HOST = process.env.HOST || '0.0.0.0'
  const PORT = process.env.PORT || 5002

  return {
    internalSecret: 'qvoFrVwvKV69VZu4YtgxJlqOnPdD5vKVhPjyZwle',
    isTest: env.indexOf('test') > -1,
    appVersion: gitVersion({ uniqueSnapshot: true }),
    rootDir: path.resolve('.'),
    host: HOST,
    port: PORT,
    env,
    defaultSize: 64,
    defaultFormat: 'png',
    maxPngSize: 1600,
    defaultPlatform: 'ios7',
    serverUrl: process.env.SERVER_URL || `http://localhost:${PORT}`,
    docsServerUrl: process.env.SWAGGER_HOST,
    maxResponseTime: 8000,
    newrelic: {
      isAvailable: process.env.NEW_RELIC_AVAILABLE === 'true',
      appName: process.env.NEW_RELIC_APP_NAME
    },
    sentry: {
      isAvailable: process.env.SENTRY_AVAILABLE === 'true',
      dsn: process.env.SENTRY_DSN
    }
  }
}
