const basicConnectionParameters = {
  autoIndex: true,
  autoReconnect: true,
  keepAlive: true,
  poolSize: 50,
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false
}

const RENDERER_DB_CREDS = process.env.RENDERER_DB_CREDS
const RENDERER_REPLICA_SET = process.env.RENDERER_REPLICA_SET

function getRendererConnectionParams () {
  if (RENDERER_REPLICA_SET) {
    return {
      replicaSet: RENDERER_REPLICA_SET || 'rs_renderer',
      readPreference: 'primary'
    }
  } else {
    return basicConnectionParameters
  }
}

export default () => ({
  db: {
    renderer: {
      credentials: RENDERER_DB_CREDS ||
        'mongodb://localhost:27017/renderer',
      primaryConnectionParameters: {
        ...getRendererConnectionParams()
      }
    }
  }
})
