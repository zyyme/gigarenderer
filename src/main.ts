/* eslint-disable import/first */
require('dotenv-flow').config({
  path: 'src/config/env',
  purge_dotenv: true
})

import appConfig from './config/app'
import { Logger, LoggerErrorInterceptor } from 'nestjs-pino'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './boot/app.module'
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { ValidationPipe } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

// newrelic should be set up before all imports
if (appConfig().newrelic.isAvailable) {
  if (!appConfig().newrelic.appName) {
    throw new Error('Specify unique human-readable Newrelic app name in env var NEW_RELIC_APP_NAME or disable Newrelic with setting config.newrelic.isAvailable to false.')
  }
  require('newrelic')
}

async function bootstrap () {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
    { bufferLogs: true }
  )

  await setupApp(app)
  const config = app.get(ConfigService)
  const logger = app.get(Logger)

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Icons renderer api')
    .setVersion('0.1')
    .addBearerAuth()
    .build()
  const swaggerDocument = SwaggerModule.createDocument(app, swaggerConfig)
  SwaggerModule.setup('/api/v1/docs', app, swaggerDocument)

  if (config.get<boolean>('SENTRY_AVAILABLE') ?? config.get<boolean>('sentry.isAvailable')) {
    require('@sentry/node').init({
      dsn: config.get<string>('SENTRY_DSN') ?? config.get<string>('sentry.dsn'),
      environment: config.get<string>('env'),
      release: config.get<string>('appVersion'),
      sendDefaultPii: true
    })
  }

  await app.listen(config.get<number>('port'), config.get<string>('host'))
  logger.log(`App started on ${await app.getUrl()}`)
}

export async function setupApp (app: NestFastifyApplication) {
  app.enableCors()
  app.useLogger(app.get(Logger))

  if (!appConfig().isTest) app.useGlobalInterceptors(new LoggerErrorInterceptor())

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transformOptions: { enableImplicitConversion: true }
  }))
}

bootstrap()
