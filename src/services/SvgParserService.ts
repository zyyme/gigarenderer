import { PlatformInfo, RenderParams, SVGSource, IconData } from '../utils'
import * as chromatism from 'chromatism'

export class SvgParserService {
  getViewBox (svg: SVGSource, width: number, height: number, platform?: PlatformInfo): string {
    if (svg.indexOf('viewBox=') !== -1) {
      return svg.split('viewBox="')[1].split('"')[0]
    } else {
      if (platform) {
        return `0 0 ${platform.size} ${platform.size}`
      }
      return `0 0 ${width} ${height}`
    }
  }

  prepareAdditionalNamespaces (svg: SVGSource) {
    const attributes = /<svg((?:.|[\r\n])*?)>/.exec(svg)[1]
    const namespaceRE = /xmlns:\S*=".*?"/g
    const namespaces = []
    let namespaceMatch
    while ((namespaceMatch = namespaceRE.exec(attributes))) {
      namespaces.push(namespaceMatch)
    }
    return namespaces.join(' ')
  }

  prepareMiscellaneousAttributes (svg: SVGSource) {
    const variousAttributesNames = ['fill-rule', 'clip-rule', 'image-rendering', 'shape-rendering', 'text-rendering', 'baseProfile']
    const processedAttributesToAdd = []

    for (const attribute of variousAttributesNames) {
      if (svg.indexOf(`${attribute}=`) !== -1) {
        processedAttributesToAdd.push(`${attribute}="` + svg.split(`${attribute}="`)[1].split('"')[0] + '"')
      }
    }

    return processedAttributesToAdd
  }

  constructReplacementSvg (additionalNamespaces: string, findViewBox: string, width: number, height: number, miscellaneousAttributes: string[]) {
    let svgHeader = `<svg xmlns="http://www.w3.org/2000/svg" ${additionalNamespaces} viewBox="${findViewBox}" width="${width}px" height="${height}px"`

    for (const attribute of miscellaneousAttributes) {
      svgHeader += ` ${attribute}`
    }
    svgHeader += '>'

    return svgHeader
  }

  replaceDefaultSvgColor (iconSource: IconData, renderParams: RenderParams) {
    let { color } = renderParams

    if (Array.isArray(color) && color.length > 0) {
      color = color[0]
    }

    iconSource.source = iconSource.source.replace(/fill="(.+?)"/gim, (m, group) => {
      if (group === 'none') {
        return `fill="${group}"`
      } else {
        return `fill="#${color}"`
      }
    })

    iconSource.source = iconSource.source.replace(/stroke="(.+?)"/gim, (m, group) => {
      if (group === 'none') {
        return `stroke="${group}"`
      } else {
        return `stroke="#${color}"`
      }
    })

    return iconSource.source.replace('<svg', `<svg fill="#${color}"`)
  }

  replaceGradientSVGColors (iconSource: IconData, renderParams: RenderParams) {
    if (renderParams.color && Array.isArray(renderParams.color) && renderParams.color.length >= 1) {
      let colorsGradient = []
      const matchers = [
        iconSource.source.match(/stop-color="(.+?)"/gim),
        iconSource.source.match(/stop-color:(#[a-fA-f0-9]{1,6})/gim)
      ]

      const search = [
        ...(matchers[0] ? matchers[0] : []),
        ...(matchers[1] ? matchers[1] : [])
      ]

      for (let matchedColor of search) {
        matchedColor = matchedColor.indexOf('=') !== -1 ? matchedColor.split('=')[1] : matchedColor.split(':')[1]
        matchedColor = matchedColor.substr(
          matchedColor.indexOf('#'), 7)
        if (colorsGradient.indexOf(matchedColor) === -1) {
          colorsGradient.push(matchedColor)
        }
      }

      colorsGradient = colorsGradient.sort((a, b) => {
        return chromatism.brightness(0, a).hsl.l - chromatism.brightness(0, b).hsl.l
      })

      const colors = this.getColorsQuad(renderParams)

      let index = 1
      iconSource.source = iconSource.source.replace(/stop-color="(.+?)"/gim, (m, group) => {
        const color = this.getGradientColorReplacement(group, colorsGradient, colors, index)
        index += 1
        return `stop-color="${color}"`
      })

      index = 1
      iconSource.source = iconSource.source.replace(/stop-color:(#[a-fA-f0-9]{1,6})/gim, (m, group) => {
        const color = this.getGradientColorReplacement(group, colorsGradient, colors, index)
        index += 1
        return `stop-color:${color}`
      })
    }
    return iconSource.source
  }

  private getGradientColorReplacement (group, colorsGradient, colors, index) {
    const { color1, color2, color3, color4 } = colors
    let color

    if (group === colorsGradient[0] || group === colorsGradient[1]) {
      if (index % 2 !== 0) {
        color = color1
      } else {
        color = color2
      }
    }

    if (group === colorsGradient[2] || group === colorsGradient[3]) {
      if (index % 2 !== 0) {
        color = color3
      } else {
        color = color4
      }
    }
    return color
  }

  getColorsQuad (renderParams) {
    let color1, color2

    if (renderParams.color.length === 1) {
      color1 = `#${renderParams.color[0]}`
      color2 = `#${renderParams.color[0]}`
    } else {
      color1 = `#${renderParams.color[0]}`
      color2 = `#${renderParams.color[1]}`
    }

    const color3 = chromatism.brightness(22, color1).hex
    const color4 = chromatism.brightness(22, color2).hex

    return { color1, color2, color3, color4 }
  }
}
