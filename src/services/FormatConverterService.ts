import { IconData, RenderFormats, RenderParams } from '../utils'
import * as PDFDocument from 'pdfkit'
import * as SVGToPDF from 'svg-to-pdfkit'
import * as sharp from 'sharp'

export class FormatConverterService {
  constructor (
    private maxPngSize: number
  ) {
  }

  async convert (icon: IconData['source'], renderParams: RenderParams): Promise<Buffer> {
    const { format } = renderParams

    if (format === RenderFormats.png) {
      return this.convertSvgToPng(icon, renderParams)
    }

    if (format === RenderFormats.pdf) {
      return this.convertSvgToPdf(icon, renderParams)
    }

    return Buffer.from(icon)
  }

  private async convertSvgToPng (icon: IconData['source'], renderParams: RenderParams): Promise<Buffer> {
    let { width } = renderParams
    const bufferSvg = Buffer.from(icon)

    if (width) {
      if (width > this.maxPngSize) width = this.maxPngSize
      return await sharp(bufferSvg)
        .resize(width)
        .png()
        .toBuffer()
    } else {
      return await sharp(bufferSvg)
        .png()
        .toBuffer()
    }
  }

  private async convertSvgToPdf (icon: IconData['source'], renderParams: RenderParams): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      const doc = new PDFDocument({
        size: [renderParams.width, renderParams.height],
        margins: 0
      })

      const buffers = []
      doc.on('data', buffers.push.bind(buffers))
      doc.on('error', (err) => reject(err))
      doc.on('end', () => {
        resolve(Buffer.concat(buffers))
      })

      SVGToPDF(
        doc, icon, 0, 0,
        {
          width: renderParams.width,
          height: renderParams.height,
          preserveAspectRatio: 'xMaxYMax [meet]'
        }
      )

      doc.end()
    })
  }
}
