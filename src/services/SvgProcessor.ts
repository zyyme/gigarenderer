import { SvgParserService } from './SvgParserService'
import { gradientStyles } from '../utils/enums'
import { IconData, RenderParams, SVGSource } from '../utils'

export class SvgProcessorService {
  constructor (
    private svgParser = new SvgParserService()
  ) {
  }

  resizeSvg (iconSource: IconData, renderParams: RenderParams): SVGSource {
    const { source, platform } = iconSource
    const { width, height } = renderParams

    const viewBox = this.svgParser.getViewBox(source, width, height, platform)

    const additionalNamespaces = this.svgParser.prepareAdditionalNamespaces(source)
    const variousAttributes = this.svgParser.prepareMiscellaneousAttributes(source)
    const svgReplacement = this.svgParser.constructReplacementSvg(additionalNamespaces, viewBox, width, height, variousAttributes)
    return source.replace(
      /<svg.+?>/,
      svgReplacement
    )
  }

  recolorSvg (iconSource: IconData, renderParams: RenderParams): SVGSource {
    let { source, platform } = iconSource
    const { color } = renderParams

    if (color && gradientStyles.indexOf(platform.apiCode) === -1) {
      source = this.svgParser.replaceDefaultSvgColor(iconSource, renderParams)
    }

    if (gradientStyles.indexOf(platform.apiCode) !== -1) {
      source = this.svgParser.replaceGradientSVGColors(iconSource, renderParams)
    }

    return source
  }
}
