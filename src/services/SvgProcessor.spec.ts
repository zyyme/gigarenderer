import { SvgProcessorService } from './SvgProcessor'
import { IconData, RenderFormats, RenderParams } from '../utils'
import { expect } from '../utils/tests'
import { beforeEach } from 'mocha'

describe('SvgProcessorService tests', () => {
  const svgProcessor = new SvgProcessorService()

  let iconSource: IconData
  let renderParams: RenderParams

  beforeEach(() => {
    iconSource = {
      _id: 'id',
      name: 'name',
      platform: {
        apiCode: 'test platform',
        seoCode: 'test platform',
        size: 64
      },
      createdAt: new Date(),
      updatedAt: new Date(),
      isAnimated: false,
      source: '<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path d="M 9 2 C 7.895 2 7 2.895 7 4 L 7 26 C 7 27.105 7.895 28 9 28 L 21 28 C 22.105 28 23 27.105 23 26 L 23 4 C 23 2.895 22.105 2 21 2 L 9 2 z M 9 4 L 21 4 L 21 25 L 17 25 C 17 25.552 16.552 26 16 26 L 14 26 C 13.448 26 13 25.552 13 25 L 9 25 L 9 4 z M 12.605469 9.2441406 C 12.541406 9.2551719 12.480125 9.2791562 12.421875 9.3164062 C 12.188875 9.4654063 12.120531 9.7748125 12.269531 10.007812 L 12.849609 10.912109 C 12.325609 11.452109 12 12.188 12 13 L 18 13 C 18 12.188 17.674391 11.452109 17.150391 10.912109 L 17.730469 10.007812 C 17.879469 9.7748125 17.812078 9.4654063 17.580078 9.3164062 C 17.348078 9.1674062 17.037672 9.2347969 16.888672 9.4667969 L 16.337891 10.326172 C 15.932891 10.124172 15.483 10 15 10 C 14.517 10 14.066109 10.125125 13.662109 10.328125 L 13.111328 9.46875 C 12.999578 9.294 12.797656 9.2110469 12.605469 9.2441406 z M 11.5 14 C 11.224 14 11 14.224 11 14.5 L 11 16.5 C 11 16.776 11.224 17 11.5 17 C 11.776 17 12 16.776 12 16.5 L 12 17 C 12 17.552 12.448 18 13 18 L 13 19.5 C 13 19.776 13.224 20 13.5 20 C 13.776 20 14 19.776 14 19.5 L 14 18 L 16 18 L 16 19.5 C 16 19.776 16.224 20 16.5 20 C 16.776 20 17 19.776 17 19.5 L 17 18 C 17.552 18 18 17.552 18 17 L 18 16.5 C 18 16.776 18.224 17 18.5 17 C 18.776 17 19 16.776 19 16.5 L 19 14.5 C 19 14.224 18.776 14 18.5 14 L 18 14 L 12 14 L 11.5 14 z"/></svg>\n'
    }
    renderParams = {
      width: 128,
      height: 128,
      color: 'ff0000',
      format: RenderFormats.png
    }
  })

  describe('resize svg', () => {
    it('adds height and width to svg tag', () => {
      const result = svgProcessor.resizeSvg(iconSource, renderParams)
      expect(result.indexOf('width="128px"')).not.eq(-1)
      expect(result.indexOf('height="128px"')).not.eq(-1)
    })

    it('retains viewbox size if present', () => {
      const result = svgProcessor.resizeSvg(iconSource, renderParams)

      expect(result.indexOf('viewBox="0 0 64 64"')).not.eq(-1)
    })

    it('adds a viewbox from platform size if no viewbox found', () => {
      iconSource.source = iconSource.source.replace('viewBox="0 0 64 64"', '')

      const result = svgProcessor.resizeSvg(iconSource, renderParams)

      expect(result.indexOf(`viewBox="0 0 ${iconSource.platform.size} ${iconSource.platform.size}"`)).not.eq(-1)
    })

    it('adds a viewbox from query size if no platform and viewbox found', () => {
      iconSource.source = iconSource.source.replace('viewBox="0 0 64 64"', '')
      iconSource.platform = null

      const result = svgProcessor.resizeSvg(iconSource, renderParams)

      expect(result.indexOf(`viewBox="0 0 ${renderParams.width} ${renderParams.height}"`)).not.eq(-1)
    })
  })

  describe('recolor svg', () => {
    describe('monotone icon recolor', () => {
      beforeEach(() => {
        iconSource = {
          _id: 'id',
          name: 'monotone icon',
          platform: {
            apiCode: 'monotone platform',
            seoCode: 'monotone platform',
            size: 64
          },
          createdAt: new Date(),
          updatedAt: new Date(),
          isAnimated: false,
          source: '<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path d="M 9 2 C 7.895 2 7 2.895 7 4 L 7 26 C 7 27.105 7.895 28 9 28 L 21 28 C 22.105 28 23 27.105 23 26 L 23 4 C 23 2.895 22.105 2 21 2 L 9 2 z M 9 4 L 21 4 L 21 25 L 17 25 C 17 25.552 16.552 26 16 26 L 14 26 C 13.448 26 13 25.552 13 25 L 9 25 L 9 4 z M 12.605469 9.2441406 C 12.541406 9.2551719 12.480125 9.2791562 12.421875 9.3164062 C 12.188875 9.4654063 12.120531 9.7748125 12.269531 10.007812 L 12.849609 10.912109 C 12.325609 11.452109 12 12.188 12 13 L 18 13 C 18 12.188 17.674391 11.452109 17.150391 10.912109 L 17.730469 10.007812 C 17.879469 9.7748125 17.812078 9.4654063 17.580078 9.3164062 C 17.348078 9.1674062 17.037672 9.2347969 16.888672 9.4667969 L 16.337891 10.326172 C 15.932891 10.124172 15.483 10 15 10 C 14.517 10 14.066109 10.125125 13.662109 10.328125 L 13.111328 9.46875 C 12.999578 9.294 12.797656 9.2110469 12.605469 9.2441406 z M 11.5 14 C 11.224 14 11 14.224 11 14.5 L 11 16.5 C 11 16.776 11.224 17 11.5 17 C 11.776 17 12 16.776 12 16.5 L 12 17 C 12 17.552 12.448 18 13 18 L 13 19.5 C 13 19.776 13.224 20 13.5 20 C 13.776 20 14 19.776 14 19.5 L 14 18 L 16 18 L 16 19.5 C 16 19.776 16.224 20 16.5 20 C 16.776 20 17 19.776 17 19.5 L 17 18 C 17.552 18 18 17.552 18 17 L 18 16.5 C 18 16.776 18.224 17 18.5 17 C 18.776 17 19 16.776 19 16.5 L 19 14.5 C 19 14.224 18.776 14 18.5 14 L 18 14 L 12 14 L 11.5 14 z"/></svg>'
        }
        renderParams = {
          width: 128,
          height: 128,
          color: 'ff0000',
          format: RenderFormats.png
        }
      })

      it('adds color if none was set', () => {
        const result = svgProcessor.recolorSvg(iconSource, renderParams)

        expect(result.indexOf('fill="#ff0000"')).not.eq(-1)
      })

      it('replaces current fill', () => {
        iconSource.source = '<?xml version="1.0"?><svg fill="#00ff00" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path d="M 9 2 C 7.895 2 7 2.895 7 4 L 7 26 C 7 27.105 7.895 28 9 28 L 21 28 C 22.105 28 23 27.105 23 26 L 23 4 C 23 2.895 22.105 2 21 2 L 9 2 z M 9 4 L 21 4 L 21 25 L 17 25 C 17 25.552 16.552 26 16 26 L 14 26 C 13.448 26 13 25.552 13 25 L 9 25 L 9 4 z M 12.605469 9.2441406 C 12.541406 9.2551719 12.480125 9.2791562 12.421875 9.3164062 C 12.188875 9.4654063 12.120531 9.7748125 12.269531 10.007812 L 12.849609 10.912109 C 12.325609 11.452109 12 12.188 12 13 L 18 13 C 18 12.188 17.674391 11.452109 17.150391 10.912109 L 17.730469 10.007812 C 17.879469 9.7748125 17.812078 9.4654063 17.580078 9.3164062 C 17.348078 9.1674062 17.037672 9.2347969 16.888672 9.4667969 L 16.337891 10.326172 C 15.932891 10.124172 15.483 10 15 10 C 14.517 10 14.066109 10.125125 13.662109 10.328125 L 13.111328 9.46875 C 12.999578 9.294 12.797656 9.2110469 12.605469 9.2441406 z M 11.5 14 C 11.224 14 11 14.224 11 14.5 L 11 16.5 C 11 16.776 11.224 17 11.5 17 C 11.776 17 12 16.776 12 16.5 L 12 17 C 12 17.552 12.448 18 13 18 L 13 19.5 C 13 19.776 13.224 20 13.5 20 C 13.776 20 14 19.776 14 19.5 L 14 18 L 16 18 L 16 19.5 C 16 19.776 16.224 20 16.5 20 C 16.776 20 17 19.776 17 19.5 L 17 18 C 17.552 18 18 17.552 18 17 L 18 16.5 C 18 16.776 18.224 17 18.5 17 C 18.776 17 19 16.776 19 16.5 L 19 14.5 C 19 14.224 18.776 14 18.5 14 L 18 14 L 12 14 L 11.5 14 z"/></svg>'

        const result = svgProcessor.recolorSvg(iconSource, renderParams)

        expect(result.indexOf('fill="#ff0000"')).not.eq(-1)
      })

      it('replaces current stroke', () => {
        iconSource.source = '<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path stroke="#00ff00" d="M 9 2 C 7.895 2 7 2.895 7 4 L 7 26 C 7 27.105 7.895 28 9 28 L 21 28 C 22.105 28 23 27.105 23 26 L 23 4 C 23 2.895 22.105 2 21 2 L 9 2 z M 9 4 L 21 4 L 21 25 L 17 25 C 17 25.552 16.552 26 16 26 L 14 26 C 13.448 26 13 25.552 13 25 L 9 25 L 9 4 z M 12.605469 9.2441406 C 12.541406 9.2551719 12.480125 9.2791562 12.421875 9.3164062 C 12.188875 9.4654063 12.120531 9.7748125 12.269531 10.007812 L 12.849609 10.912109 C 12.325609 11.452109 12 12.188 12 13 L 18 13 C 18 12.188 17.674391 11.452109 17.150391 10.912109 L 17.730469 10.007812 C 17.879469 9.7748125 17.812078 9.4654063 17.580078 9.3164062 C 17.348078 9.1674062 17.037672 9.2347969 16.888672 9.4667969 L 16.337891 10.326172 C 15.932891 10.124172 15.483 10 15 10 C 14.517 10 14.066109 10.125125 13.662109 10.328125 L 13.111328 9.46875 C 12.999578 9.294 12.797656 9.2110469 12.605469 9.2441406 z M 11.5 14 C 11.224 14 11 14.224 11 14.5 L 11 16.5 C 11 16.776 11.224 17 11.5 17 C 11.776 17 12 16.776 12 16.5 L 12 17 C 12 17.552 12.448 18 13 18 L 13 19.5 C 13 19.776 13.224 20 13.5 20 C 13.776 20 14 19.776 14 19.5 L 14 18 L 16 18 L 16 19.5 C 16 19.776 16.224 20 16.5 20 C 16.776 20 17 19.776 17 19.5 L 17 18 C 17.552 18 18 17.552 18 17 L 18 16.5 C 18 16.776 18.224 17 18.5 17 C 18.776 17 19 16.776 19 16.5 L 19 14.5 C 19 14.224 18.776 14 18.5 14 L 18 14 L 12 14 L 11.5 14 z"/></svg>'

        const result = svgProcessor.recolorSvg(iconSource, renderParams)

        expect(result.indexOf('stroke="#ff0000"')).not.eq(-1)
      })
    })

    describe('coloured icon recolor', () => {
      beforeEach(() => {
        iconSource = {
          _id: 'id',
          name: 'coloured icon',
          platform: {
            apiCode: 'coloured platform',
            seoCode: 'coloured platform',
            size: 64
          },
          createdAt: new Date(),
          updatedAt: new Date(),
          isAnimated: false,
          source: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path fill="#4788c7" d="M25.5 6.207l9.647 9.647c.194.194.513.194.707 0v0c.194-.194.194-.513 0-.707L26.207 5.5c-.194-.194-.513-.194-.707 0v0C25.306 5.694 25.306 6.013 25.5 6.207zM23.5 8.207l9.647 9.647c.194.194.513.194.707 0v0c.194-.194.194-.513 0-.707L24.207 7.5c-.194-.194-.513-.194-.707 0v0C23.306 7.694 23.306 8.013 23.5 8.207zM21.5 10.207l9.647 9.647c.194.194.513.194.707 0l0 0c.194-.194.194-.513 0-.707L22.207 9.5c-.194-.194-.513-.194-.707 0l0 0C21.306 9.694 21.306 10.013 21.5 10.207zM19.5 12.207l9.647 9.647c.194.194.513.194.707 0l0 0c.194-.194.194-.513 0-.707L20.207 11.5c-.194-.194-.513-.194-.707 0l0 0C19.306 11.694 19.306 12.013 19.5 12.207zM17.5 14.207l9.647 9.647c.194.194.513.194.707 0l0 0c.194-.194.194-.513 0-.707L18.207 13.5c-.194-.194-.513-.194-.707 0l0 0C17.306 13.694 17.306 14.013 17.5 14.207zM15.5 16.207l9.647 9.647c.194.194.513.194.707 0l0 0c.194-.194.194-.513 0-.707L16.207 15.5c-.194-.194-.513-.194-.707 0l0 0C15.306 15.694 15.306 16.013 15.5 16.207zM13.5 18.207l9.647 9.647c.194.194.513.194.707 0l0 0c.194-.194.194-.513 0-.707L14.207 17.5c-.194-.194-.513-.194-.707 0l0 0C13.306 17.694 13.306 18.013 13.5 18.207zM11.5 20.207l9.647 9.647c.194.194.513.194.707 0l0 0c.194-.194.194-.513 0-.707L12.207 19.5c-.194-.194-.513-.194-.707 0l0 0C11.306 19.694 11.306 20.013 11.5 20.207zM9.5 22.207l9.647 9.647c.194.194.513.194.707 0l0 0c.194-.194.194-.513 0-.707L10.207 21.5c-.194-.194-.513-.194-.707 0l0 0C9.306 21.694 9.306 22.013 9.5 22.207zM7.5 24.207l9.647 9.647c.194.194.513.194.707 0h0c.194-.194.194-.513 0-.707L8.207 23.5c-.194-.194-.513-.194-.707 0h0C7.306 23.694 7.306 24.013 7.5 24.207zM5.5 26.207l9.647 9.647c.194.194.513.194.707 0h0c.194-.194.194-.513 0-.707L6.207 25.5c-.194-.194-.513-.194-.707 0h0C5.306 25.694 5.306 26.013 5.5 26.207z"/><path fill="#b6dcfe" d="M12.819,38.222c-0.146,0-0.288-0.053-0.399-0.149L2.191,29.164c-0.928-0.93-0.928-2.481,0.025-3.434 L25.791,2.216C26.253,1.754,26.867,1.5,27.52,1.5s1.267,0.254,1.729,0.716l8.856,10.17c0.209,0.24,0.196,0.604-0.029,0.83 c-0.115,0.115-0.268,0.178-0.43,0.178s-0.315-0.063-0.43-0.178L29.84,5.84l-7.885,7.819L5.839,29.777l7.41,7.41 c0.236,0.236,0.236,0.622,0,0.858C13.134,38.159,12.981,38.222,12.819,38.222z"/><path fill="#4788c7" d="M27.52,2c0.509,0,0.988,0.194,1.353,0.548l8.855,10.167c0.037,0.043,0.035,0.108-0.005,0.148 c-0.021,0.021-0.047,0.032-0.077,0.032c-0.03,0-0.056-0.011-0.077-0.032l-7.024-7.024l-0.704-0.704l-0.707,0.701l-7.534,7.472 L5.839,29.07l-0.707,0.707l0.707,0.707l7.056,7.056c0.042,0.042,0.042,0.109,0,0.151c-0.026,0.026-0.055,0.031-0.076,0.031 c-0.026,0-0.05-0.009-0.07-0.026L2.548,28.811c-0.736-0.76-0.729-1.977,0.021-2.727L26.145,2.57C26.512,2.202,27.001,2,27.52,2 M27.52,1c-0.754,0-1.507,0.288-2.082,0.863L1.862,25.376c-1.15,1.15-1.15,3.014,0,4.164l10.23,8.91 c0.209,0.182,0.468,0.272,0.727,0.272c0.284,0,0.567-0.109,0.783-0.324h0c0.432-0.432,0.432-1.133,0-1.565l-7.056-7.056 l15.762-15.762l7.531-7.469l7.024,7.024c0.216,0.216,0.5,0.325,0.784,0.325s0.567-0.108,0.784-0.325 c0.412-0.412,0.435-1.073,0.052-1.512l-8.88-10.195C29.027,1.287,28.274,1,27.52,1L27.52,1z"/></svg>'
        }
        renderParams = {
          width: 128,
          height: 128,
          color: 'ff0000',
          format: RenderFormats.png
        }
      })

      it('replaces fill on coloured icon', () => {
        const result = svgProcessor.recolorSvg(iconSource, renderParams)
        expect((result.match(/fill="#ff0000"/g) || []).length).eq(4)
      })
    })

    describe('gradient icon recolor', () => {
      beforeEach(() => {
        iconSource = {
          _id: 'id',
          name: 'gradient icon',
          platform: {
            apiCode: 'nolan',
            seoCode: 'nolan',
            size: 64
          },
          createdAt: new Date(),
          updatedAt: new Date(),
          isAnimated: false,
          source: '<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 64 64" width="64px" height="64px"><linearGradient id="NDrQZOId9rIQr5R5W~UCTa" x1="31" x2="31" y1="45.667" y2="57.376" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stop-color="#6dc7ff"/><stop offset="1" stop-color="#e6abff"/></linearGradient><path fill="url(#NDrQZOId9rIQr5R5W~UCTa)" d="M31 48A3 3 0 1 0 31 54A3 3 0 1 0 31 48Z"/><linearGradient id="NDrQZOId9rIQr5R5W~UCTb" x1="32" x2="32" y1="4.333" y2="60.036" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stop-color="#1a6dff"/><stop offset="1" stop-color="#c822ff"/></linearGradient><path fill="url(#NDrQZOId9rIQr5R5W~UCTb)" d="M51,24v-4c0-1.103-1-2-2-2v-7c0-2.757-2.243-5-5-5H18c-2.757,0-5,2.243-5,5v42 c0,2.757,2.243,5,5,5h26c2.757,0,5-2.243,5-5V38c1,0,2-0.897,2-2v-4c0-1.103-1-2-2-2v-4C50,26,51,25.103,51,24z M47,53 c0,1.657-1.343,3-3,3H18c-1.657,0-3-1.343-3-3V11c0-1.657,1.343-3,3-3h26c1.657,0,3,1.343,3,3V53z"/><linearGradient id="NDrQZOId9rIQr5R5W~UCTc" x1="31" x2="31" y1="4.333" y2="60.036" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stop-color="#1a6dff"/><stop offset="1" stop-color="#c822ff"/></linearGradient><path fill="url(#NDrQZOId9rIQr5R5W~UCTc)" d="M25,40h2v4h2v-4h4v4h2v-4h2V28H25V40z M27,30h8v8h-8V30z"/><linearGradient id="NDrQZOId9rIQr5R5W~UCTd" x1="40" x2="40" y1="4.333" y2="60.036" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stop-color="#1a6dff"/><stop offset="1" stop-color="#c822ff"/></linearGradient><path fill="url(#NDrQZOId9rIQr5R5W~UCTd)" d="M39 28H41V37H39z"/><linearGradient id="NDrQZOId9rIQr5R5W~UCTe" x1="22" x2="22" y1="4.333" y2="60.036" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stop-color="#1a6dff"/><stop offset="1" stop-color="#c822ff"/></linearGradient><path fill="url(#NDrQZOId9rIQr5R5W~UCTe)" d="M21 28H23V37H21z"/><linearGradient id="NDrQZOId9rIQr5R5W~UCTf" x1="31" x2="31" y1="4.333" y2="60.036" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stop-color="#1a6dff"/><stop offset="1" stop-color="#c822ff"/></linearGradient><path fill="url(#NDrQZOId9rIQr5R5W~UCTf)" d="M36.293,19.646l-1.672,1.672C33.613,20.513,32.361,20,31,20s-2.613,0.513-3.621,1.319 l-1.672-1.672l-1.414,1.414l1.729,1.729C25.377,23.755,25,24.877,25,26h2c0-2,1.794-3.912,4-3.912c1.068,0,2.073,0.504,2.828,1.26 C34.584,24.104,34.999,25,34.999,26H37c0-1.123-0.377-2.245-1.022-3.21l1.729-1.729L36.293,19.646z"/><linearGradient id="NDrQZOId9rIQr5R5W~UCTg" x1="31" x2="31" y1="4.333" y2="60.036" gradientUnits="userSpaceOnUse" spreadMethod="reflect"><stop offset="0" stop-color="#1a6dff"/><stop offset="1" stop-color="#c822ff"/></linearGradient><path fill="url(#NDrQZOId9rIQr5R5W~UCTg)" d="M27 10H35V12H27z"/></svg>'
        }
        renderParams = {
          width: 128,
          height: 128,
          color: ['ff0000', '0000ff'],
          format: RenderFormats.png
        }
      })

      it('recolors gradient when two colors provided', () => {
        const result = svgProcessor.recolorSvg(iconSource, renderParams)

        expect(result.indexOf('stop-color="#ff0000"')).not.eq(-1)
        expect(result.indexOf('stop-color="#0000ff"')).not.eq(-1)
      })

      it('does not do anythng when one color provided as string', () => {
        renderParams.color = 'ff0000'
        const result = svgProcessor.recolorSvg(iconSource, renderParams)

        expect(result).eq(iconSource.source)
      })

      it('does not do anythng when one color provided in array', () => {
        renderParams.color = ['ff0000']
        const result = svgProcessor.recolorSvg(iconSource, renderParams)

        expect(result).eq(iconSource.source)
      })

      it('applies gradient of two first colors if more are provided', () => {
        renderParams.color = ['ff0000', '00ff00', '0000ff']
        const result = svgProcessor.recolorSvg(iconSource, renderParams)

        expect(result.indexOf('stop-color="#ff0000"')).not.eq(-1)
        expect(result.indexOf('stop-color="#00ff00"')).not.eq(-1)
        expect(result.indexOf('stop-color="#0000ff"')).eq(-1)
      })
    })
  })
})
