import * as sharp from 'sharp'
import { RenderParams } from '../utils'

export class PngFormatterService {
  constructor (
    private maxPngSize: number
  ) {
  }

  async resizePng (png: Buffer, renderParams: RenderParams): Promise<Buffer> {
    let { width, height } = renderParams

    if (width && height) {
      if (width > this.maxPngSize) width = this.maxPngSize
      return await sharp(png)
        .resize(width)
        .png()
        .toBuffer()
    } else {
      return png
    }
  }
}
