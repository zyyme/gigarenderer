import { Controller, HttpCode, Post, UseInterceptors } from '@nestjs/common'
import { PlainConverterService } from './plain-converter.service'
import { RenderByCustomSvgCommand } from './dto/renderByCustomSvgCommand'
import * as dayjs from 'dayjs'
import { getCommonCustomHeaders } from '../../utils/getCommonCustomHeaders'
import { ApiBody, ApiTags } from '@nestjs/swagger'
import { RenderByCustomSvgCommandDto } from './dto/renderByCustomSvgCommandDto'
import { RenderResponseHeaderInterceptor } from '../../utils/interceptors'
import { ValidateBody } from '../../utils/decorators/ValidateBody'
import { PlainConverterPipe } from '../../utils/pipes'

@ApiTags('Plain converter')
@Controller('plainConverter')
export class PlainConverterController {
  constructor (private readonly plainConverterService: PlainConverterService) {}

  @Post()
  @ApiBody({ type: RenderByCustomSvgCommandDto })
  @UseInterceptors(RenderResponseHeaderInterceptor)
  @HttpCode(200)
  async plainConverter (
    @ValidateBody(RenderByCustomSvgCommandDto, PlainConverterPipe) command: RenderByCustomSvgCommand
  ) {
    const renderResult = await this.plainConverterService.convert(command)

    const customHeaders = this.getCustomHeaders(command)

    return { data: renderResult, headers: customHeaders }
  }

  private getCustomHeaders (metadata: RenderByCustomSvgCommand) {
    const commonHeaders = getCommonCustomHeaders(metadata)

    return {
      ...commonHeaders,
      Expires: dayjs().add(7, 'day').format('ddd, DD MMM YYYY HH:mm:ss')
    }
  }
}
