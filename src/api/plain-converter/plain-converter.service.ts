import { Injectable } from '@nestjs/common'
import { RenderByCustomSvgCommand } from './dto/renderByCustomSvgCommand'
import { IconData, RenderParams } from '../../utils'
import { FormatConverterService, SvgProcessorService } from '../../services'

interface AdaptedQuery {
  iconData: IconData
  renderParams: RenderParams
}

@Injectable()
export class PlainConverterService {
  constructor (
    private readonly svgProcessorService: SvgProcessorService,
    private readonly formatConverterService: FormatConverterService,
    private readonly defaultSize: number
  ) {
  }

  async convert (query: RenderByCustomSvgCommand) {
    const { iconData, renderParams } = this.adaptQuery(query)

    if (renderParams.width) {
      iconData.source = this.svgProcessorService.resizeSvg(iconData, renderParams)
    }

    if (renderParams.color) {
      iconData.source = this.svgProcessorService.recolorSvg(iconData, renderParams)
    }

    if (renderParams.format) {
      return this.formatConverterService.convert(iconData.source, renderParams)
    }

    return iconData.source
  }

  // todo: remove
  adaptQuery (query: RenderByCustomSvgCommand): AdaptedQuery {
    const renderParams: RenderParams = {
      width: query.size,
      height: query.size,
      color: query.color,
      format: query.format
    }

    const iconData: IconData = {
      _id: query.id,
      name: 'custom',
      platform: {
        apiCode: 'custom',
        seoCode: 'custom',
        size: this.defaultSize
      },
      createdAt: new Date(),
      updatedAt: new Date(),
      isAnimated: false,
      source: query.svg
    }

    return { iconData, renderParams }
  }
}
