import { Module } from '@nestjs/common'
import { PlainConverterService } from './plain-converter.service'
import { PlainConverterController } from './plain-converter.controller'
import { ConfigService } from '@nestjs/config'
import { FormatConverterService, SvgProcessorService } from '../../services'

@Module({
  imports: [SvgProcessorService, FormatConverterService],
  controllers: [PlainConverterController],
  providers: [
    PlainConverterService,
    SvgProcessorService,
    {
      provide: FormatConverterService,
      useFactory: (config: ConfigService) => {
        return new FormatConverterService(config.get<number>('maxPngSize'))
      },
      inject: [ConfigService]
    },
    ConfigService,
    {
      provide: PlainConverterService,
      useFactory: (
        svgProcessorService: SvgProcessorService,
        formatConverterService: FormatConverterService,
        config: ConfigService
      ) => {
        return new PlainConverterService(
          svgProcessorService,
          formatConverterService,
          config.get<number>('defaultSize'))
      },
      inject: [SvgProcessorService, FormatConverterService, ConfigService]
    }
  ]
})
export class PlainConverterModule {}
