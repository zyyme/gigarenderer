import { RenderFormats } from '../../../utils'
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsEnum, IsInt, IsNotEmpty, IsOptional, IsPositive, IsString, Matches } from 'class-validator'

export class RenderByCustomSvgCommandDto {
  @ApiPropertyOptional({
    description: 'Id of icon for accounting of limit usage after recoloring/resizing icon on site.',
    example: '98151'
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  id?: string

  @ApiProperty({
    description: 'SVG string to process.',
    example: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M15 46h20c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2H15c-1.1 0-2 .9-2 2v38C13 45.1 13.9 46 15 46zM22 43L28 43M13 9L37 9M13 40L37 40"/><path fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M27 28h-4c-.6 0-1-.4-1-1v-6h6v6C28 27.6 27.6 28 27 28zM31 21L31 26M19 21L19 26M23 28L23 31M27 28L27 31M22 21c0-1.7 1.3-3 3-3s3 1.3 3 3"/><path fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M26.5 18.5L27.5 16.5M23.5 18.5L22.5 16.5"/></svg>'
  })
  @IsString()
  @IsNotEmpty()
  svg: string

  @ApiProperty({ enum: [RenderFormats.png, RenderFormats.pdf, RenderFormats.svg] })
  @IsEnum([RenderFormats.png, RenderFormats.pdf, RenderFormats.svg])
  format?: RenderFormats.png | RenderFormats.pdf | RenderFormats.svg

  @ApiPropertyOptional({
    example: 100
  })
  @IsOptional()
  @IsInt()
  @IsPositive()
  size?: number

  @ApiPropertyOptional({
    description: 'Hex representation of color.',
    example: '00ffaa'
  })
  @IsOptional()
  @IsString()
  @Matches('([a-fA-F0-9]{6},?){1,2}')
  color?: string
}
