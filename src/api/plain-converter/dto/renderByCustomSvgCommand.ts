import { RenderFormats } from '../../../utils'

export interface RenderByCustomSvgCommand {
  id?: string
  svg: string
  format: RenderFormats.png | RenderFormats.pdf | RenderFormats.svg
  size: number
  color?: string
}
