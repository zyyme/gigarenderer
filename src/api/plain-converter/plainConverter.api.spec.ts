import { ConfigModule } from '@nestjs/config'
import { LoggerModule } from 'nestjs-pino'
import { PlainConverterService } from './plain-converter.service'
import { PlainConverterController } from './plain-converter.controller'
import { expect, mockBeforeEach, sinon } from '../../utils/tests'
import { Test } from '@nestjs/testing'
import { anything, deepEqual, instance, verify, when } from 'ts-mockito'
import { setupAndRunTestApp } from '../../../test/setupAndRunTestApp'
import { ContentTypeFormats, RenderFormats } from '../../utils'
import config from '../../config'
import { HttpStatus } from '@nestjs/common'
import * as dayjs from 'dayjs'

const acceptedFormats = [RenderFormats.png, RenderFormats.pdf, RenderFormats.svg]

describe('Plain converter API', () => {
  let testApp
  const PlainConverterServiceMock: PlainConverterService = mockBeforeEach(PlainConverterService)

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [
        LoggerModule.forRoot(),
        ConfigModule.forRoot({
          isGlobal: true,
          load: [config]
        })
      ],
      providers: [PlainConverterService],
      controllers: [PlainConverterController]
    })
      .overrideProvider(PlainConverterService)
      .useValue(instance(PlainConverterServiceMock))
      .compile()

    testApp = await setupAndRunTestApp(module)
  })

  describe('POST /plainConverter', () => {
    const now = new Date()
    const imageBuffer = Buffer.from('image')
    let body

    beforeEach(() => {
      body = {
        id: 'id',
        svg: '<svg></svg>',
        format: RenderFormats.png as const,
        size: 256,
        color: 'ff0000'
      }

      when(PlainConverterServiceMock.convert(anything()))
        .thenResolve(imageBuffer)
    })

    it('parses full request correctly and pass it to service', async () => {
      const res = await testApp.inject({
        method: 'POST',
        url: '/plainConverter',
        body
      })

      expect(res.statusCode).equal(HttpStatus.OK)
      verify(PlainConverterServiceMock.convert(
        deepEqual(body)
      )).once()
    })

    describe('sets correct headers', () => {
      it('when id was passed', async () => {
        const res = await testApp.inject({
          method: 'POST',
          url: '/plainConverter',
          body: { ...body, id: 'icon id' }
        })

        expect(res.headers['content-type']).equal(ContentTypeFormats[body.format])
        expect(res.headers['last-modified']).equal(dayjs(now).format('ddd, DD MMM YYYY HH:mm:ss'))
        expect(res.headers['icon-id']).equal('icon id')
        expect(res.headers['icon-size']).equal(body.size)
        expect(res.headers['icon-format']).equal(body.format)
        expect(res.headers.expires).equal(dayjs(now).add(7, 'day').format('ddd, DD MMM YYYY HH:mm:ss'))
      })

      it('when id was not passed', async () => {
        const res = await testApp.inject({
          method: 'POST',
          url: '/plainConverter',
          body: { svg: 'svg', format: RenderFormats.png as const }
        })

        expect(res.headers['content-type']).equal(ContentTypeFormats[body.format])
        expect(res.headers['last-modified']).equal(dayjs(now).format('ddd, DD MMM YYYY HH:mm:ss'))
        // eslint-disable-next-line no-unused-expressions
        expect(res.headers['icon-id']).undefined
        // eslint-disable-next-line no-unused-expressions
        expect(res.headers['icon-size']).undefined
        // eslint-disable-next-line no-unused-expressions
        expect(res.headers['icon-format']).undefined
        expect(res.headers.expires).equal(dayjs(now).add(7, 'day').format('ddd, DD MMM YYYY HH:mm:ss'))
      })

      it('and sets content-type for different formats', async () => {
        for (const format of acceptedFormats) {
          const res = await testApp.inject({
            method: 'POST',
            url: '/plainConverter',
            body: { ...body, format }
          })

          expect(res.headers['content-type']).equal(ContentTypeFormats[format])
          // eslint-disable-next-line no-unused-expressions
          expect(res.headers['content-type']).not.undefined
        }
      })
    })

    describe('returns BAD_REQUEST when', () => {
      it('svg is missing', async () => {
        const res = await testApp.inject({
          method: 'POST',
          url: '/plainConverter',
          body: {
            ...body,
            svg: undefined
          }
        })

        expect(res.statusCode).equal(HttpStatus.BAD_REQUEST)
      })

      it('svg is empty', async () => {
        const res = await testApp.inject({
          method: 'POST',
          url: '/plainConverter',
          body: {
            ...body,
            svg: ''
          }
        })

        expect(res.statusCode).equal(HttpStatus.BAD_REQUEST)
      })

      it('id is set but empty', async () => {
        const res = await testApp.inject({
          method: 'POST',
          url: '/plainConverter',
          body: {
            ...body,
            id: ''
          }
        })

        expect(res.statusCode).equal(HttpStatus.BAD_REQUEST)
      })

      it('when color is set but has not hex-color format', async () => {
        const wrongColors = ['aabbc', 'unknown', 'RRffbb', '------', '']

        for (const color of wrongColors) {
          const res = await testApp.inject({
            method: 'POST',
            url: '/plainConverter',
            body: {
              ...body,
              color
            }
          })

          expect(res.statusCode).equal(HttpStatus.BAD_REQUEST)
        }
      })

      it('when size value is not positive or has floating point', async () => {
        const wrongSizes = [0, -1, 10.5]

        for (const size of wrongSizes) {
          const res = await testApp.inject({
            method: 'POST',
            url: '/plainConverter',
            body: {
              ...body,
              size
            }
          })

          expect(res.statusCode).equal(HttpStatus.BAD_REQUEST)
        }
      })

      it('when format has unsupported value', async () => {
        const wrongFormats = Object.values(RenderFormats)
          .filter(f => !acceptedFormats.includes(f))
          .concat(['unknown', ''] as any)

        for (const format of wrongFormats) {
          const res = await testApp.inject({
            method: 'POST',
            url: '/plainConverter',
            body: {
              ...body,
              format
            }
          })

          expect(res.statusCode).equal(HttpStatus.BAD_REQUEST)
        }
      })
    })
  })
})
