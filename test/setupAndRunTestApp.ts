import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify'
import { TestingModule } from '@nestjs/testing'
import { setupApp } from '../src/main'

export async function setupAndRunTestApp (module: TestingModule) {
  const app = module.createNestApplication<NestFastifyApplication>(new FastifyAdapter())
  await setupApp(app)
  await app.init()
  await app.getHttpAdapter().getInstance().ready()
  return app
}
