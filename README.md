# Simple SVG Converter

This app allows users to edit their custom SVGs, containing features like recoloring, resizing, converting to other formats
(png, pdf, eps are supported now).

### Built with

This app is built using [NestJS](https://nestjs.com) with Fastify backend engine. It uses [sharp](https://sharp.pixelplumbing.com) and [svg-to-pdfkit](https://www.npmjs.com/package/svg-to-pdfkit) for converting SVGs,
[pino](https://github.com/pinojs/pino) for logging, and [mocha](https://mochajs.org), [sinon](https://sinonjs.org) and Yandex.Tank for testing. It also uses [ESLint](https://eslint.org) and [Prettier](https://prettier.io) in order to maintain the high quality of the code.

## Getting started
### Installation guide

In order to run locally perform these commands:

```shell
git clone git@gitlab.com:zyyme/gigarenderer.git

npm ci

npm run start
```

And you will be good to go.

In case if you want to run this app in container, you can use Dockerfile provided:

```shell
git clone git@gitlab.com:zyyme/svg-converter.git

docker build -t svg-converter .

docker run svg-converter
```

### Usage guide

The app is available on port 5002. In order to edit SVG, you need to send POST request to `/plainConverter`
with SVG and edititng params in body. Example of request body:

```json5
{
  "svg": "<svg></svg>",
  "format": "png", // or svg, eps, pdf
  "size": 64,
  "color": "ff0000"
}
```

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feat/new-feature`)
3. Commit your Changes (`git commit -m 'feat: new feature'`)
4. Push to the Branch (`git push origin feat/new-feature`)
5. Open a Pull Request

## License

Distributed under the MIT License.

## Contact

Vladislav Levchenko - [telegram](https://t.me/zyyme) - v.levchenko@innopolis.university

Daniil Sinelnik - [telegram](https://t.me/s1ngleee) - d.sinelnik@innopolis.university

Project Link: [https://gitlab.com/zyyme/svg-converter](https://gitlab.com/zyyme/svg-converter)
