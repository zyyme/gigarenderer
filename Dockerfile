FROM node:16

ENV NODE_VERSION 16.17.0

RUN npm install --global npm@8.5.0

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm ci

COPY ./ ./
RUN npm run build

CMD npm run start
